# Generated by Django 2.2.1 on 2021-04-27 20:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('employee_management', '0003_auto_20210427_1440'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pilots',
            name='visa_expiration_date',
            field=models.DateTimeField(max_length=35, verbose_name='Visa Expiration Date'),
        ),
    ]
