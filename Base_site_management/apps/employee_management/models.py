from django.db import models
from django.db.models.deletion import CASCADE, SET_NULL
from datetime import *

# Create your models here.

class Base(models.Model):
    base_name                       = models.CharField(max_length=15, verbose_name="Base Name")
    base_location                   = models.CharField(max_length=35, verbose_name="Base Location")
    
    def __str__(self):
        return self.base_name
    
    
class Pilot(models.Model):
    first_name                      = models.CharField(max_length=35, verbose_name="First Name")
    middle_name                     = models.CharField(max_length=35, verbose_name="Middle Name")
    last_name                       = models.CharField(max_length=35, verbose_name="Last Name")
    birth_date                      = models.DateField(max_length=35, verbose_name="Birth Date")
    visa_expiration_date            = models.DateTimeField(max_length=15, verbose_name="Visa Expiration Date")
    current_base_assigned           = models.ForeignKey(Base, null=False, blank=False, on_delete=models.CASCADE, verbose_name="Current Base Assigned")
    remaining_days_on_current_base  = models.PositiveIntegerField(verbose_name="Remaining Days on Current Base")
    
    def FullName(self):
        str="{0} {1}"
        return str.format(self.first_name, self.last_name)
    
    def __str__(self):
        return self.FullName()  
          
    
class FlightAttendant(models.Model):
    first_name                      = models.CharField(max_length=35, verbose_name="First Name")
    middle_name                     = models.CharField(max_length=35, verbose_name="Middle Name")
    last_name                       = models.CharField(max_length=35, verbose_name="Last Name")
    birth_date                      = models.DateField(max_length=35, verbose_name="Birth Date")
    visa_expiration_date            = models.DateField(max_length=35, verbose_name="Visa Expiration Date")
    current_base_assigned           = models.ForeignKey(Base, null=False, blank=False, on_delete=models.CASCADE, verbose_name="Current Base Assigned")
    remaining_days_on_current_base  = models.IntegerField(verbose_name="Remaining Days on Current Base")
    
    def FullName(self):
        str="{0} {1}, {2}"
        return str.format(self.first_name, self.middle_name, self.last_name)
    
    def __str__(self):
        return self.FullName()
    
    
class Engineer(models.Model):
    first_name                      = models.CharField(max_length=35, verbose_name="First Name")
    middle_name                     = models.CharField(max_length=35, verbose_name="Middle Name")
    last_name                       = models.CharField(max_length=35, verbose_name="Last Name")
    birth_date                      = models.DateField(max_length=35, verbose_name="Birth Date")
    visa_expiration_date            = models.DateField(max_length=35, verbose_name="Visa Expiration Date")
    current_base_assigned           = models.ForeignKey(Base, null=False, blank=False, on_delete=models.CASCADE, verbose_name="Current Base Assigned")
    remaining_days_on_current_base  = models.IntegerField(verbose_name="Remaining Days on Current Base")
    
    def FullName(self):
        str="{0} {1}, {2}"
        return str.format(self.first_name, self.middle_name, self.last_name)
    
    def __str__(self):
        return self.FullName()
    
    
class SiteManager(models.Model):
    first_name                      = models.CharField(max_length=35, verbose_name="First Name")
    middle_name                     = models.CharField(max_length=35, verbose_name="Middle Name")
    last_name                       = models.CharField(max_length=35, verbose_name="Last Name")
    birth_date                      = models.DateField(max_length=35, verbose_name="Birth Date")
    visa_expiration_date            = models.DateField(max_length=35, verbose_name="Visa Expiration Date")
    current_base_assigned           = models.ForeignKey(Base, null=False, blank=False, on_delete=models.CASCADE, verbose_name="Current Base Assigned")
    remaining_days_on_current_base  = models.IntegerField(verbose_name="Remaining Days on Current Base")
    
    '''def FullName(self):
        str="{0} {1}, {2}"
        return str.format(self.first_name, self.middle_name, self.last_name)'''
    
    def __str__(self):
        return self.visa_expiration_date  
     