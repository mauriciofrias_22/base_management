from django.contrib import admin
from Base_site_management.apps.employee_management.models import *


# Register your models here.

admin.site.site_header = 'Base Site Management'

admin.site.register(Base)
admin.site.register(Pilot)
admin.site.register(FlightAttendant)
admin.site.register(Engineer)
admin.site.register(SiteManager)
    